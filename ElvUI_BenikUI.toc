﻿## Interface: 50400
## Author: Benik
## Version: 0.37beta
## Title: |cff1784d1ElvUI|r_BenikUI
## Notes: an ElvUI 6+ edit
## RequiredDeps: ElvUI
## DefaultState: Enabled
## OptionalDeps: SharedMedia

media\load_media.xml
locales\load_locales.xml
core\load_core.xml
layout\layout.lua
modules\load_modules.xml