﻿-- English localization file for enUS
local AceLocale = LibStub:GetLibrary("AceLocale-3.0");
local L = AceLocale:NewLocale("ElvUI", "enUS");
if not L then return; end

L["Game Menu"] = true
L["Toggle Tokens"] = true
L["Toggle Dashboard"] = true
L["ShiftClick to toggle chat"] = true
L["Click :"] = true
L["MouseWheel :"] = true
L["Empty Frames"] = true
L["Change the Empty frames height (Player and Target)."] = true
L["Show the Empty frames (Player and Target)."] = true
L["Portraits"] = true
L["Change the detached portraits width (Player and Target)"] = true
L["Change the detached portraits height (Player and Target)"] = true
L["Shadow"] = true
L["Add shadow under the portraits (Player and Target)"] = true