ElvUI_BenikUI
=============
an external ElvUI edit

Due to early beta stage, the ui will only be available in GitHub: https://github.com/Benik/ElvUI_BenikUI
- Current version 0.25beta (Mar 21, 2014)

- Please run the installer when prompted.

- Enhancements:

> Decors all ElvUI frames.

> Adds a bar in player and target like TukUI does that can be used to place hp/power text etc. Mouse over can show rep or xp.

> Enhanced Dashboard that shows ms, fps, memory, durability, volume. Volume can be adjusted by left/right click - mouseover the speaker icon.

> Tokens Dashboard based on AsphyxiaUI.

> Detached portraits - player and target (see below for issues).

> Buttons beside the chat datatexts, that can show/hide the previous dahboards (left chat) and Game menu - ElvUI options (right side).

> 2 Small mouseovered buttons on the actionbar decor that can show/hide bar 3 and 5 (probably I will remove them from there, too "dangerous" for clickers :P)

> All options are in light blue color inside ElvUI options; currently in actionbars, unitframes, player and target portraits.

- Compatibility:
S&L and most of ElvUI plugins that I could test.

- Log:
Everything seems smooth so far except:
Target detached portrait still ain't working, so I don't recommend enabling it. Well, enabling it works but after a reload it gets messed up. Player detached portrait is working fine.

I will appreciate any help on testing or solving issues.

Thanks :D 
